#ifndef CASHIER_H
#define CASHIER_H

#include <QObject>
#include <QDate>

class Cashier
{
    int type;
    double sum;
    QString target;
    QDate date;

public:
    Cashier();
    Cashier(int type, double sum,
            QString target, QDate date);

    void setType(int type);
    void setSum(double sum);
    void setTarget(QString target);
    void setDate(QDate date);

    int getType();
    double getSum();
    QString getTarget();
    QDate getDate();
};

#endif // CASHIER_H
