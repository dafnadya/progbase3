#include "Cashier.h"

Cashier::Cashier()
{
}

Cashier::Cashier(int type, double sum,
QString target, QDate date) {
    this->type = type;
    this->sum = sum;
    this->target = target;
    this->date = date;
}

void Cashier::setType(int type) {
    this->type = type;
}

void Cashier::setSum(double sum) {
    this->sum = sum;
}

void Cashier::setTarget(QString target) {
    this->target = target;
}

void Cashier::setDate(QDate date) {
    this->date = date;
}

int Cashier::getType() {
    return type;
}

double Cashier::getSum() {
    return sum;
}

QString Cashier::getTarget() {
    return target;
}

QDate Cashier::getDate() {
    return date;
}
