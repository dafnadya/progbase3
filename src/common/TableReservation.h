#ifndef TABLERESERVATION_H
#define TABLERESERVATION_H

#include <QDateTime>
#include <QMetaType>

class TableReservation
{
    int id;
    int table;
    QTime time;
    QDate date;

public:
    TableReservation();

    void setId(int id);
    void setTable(int table);
    void setTime(QTime time);
    void setDate(QDate date);

    int getId();
    int getTable();
    QTime getTime();
    QDate getDate();
};

Q_DECLARE_METATYPE(TableReservation)

#endif // TABLERESERVATION_H
