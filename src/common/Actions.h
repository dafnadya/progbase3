#ifndef ACTIONS_H
#define ACTIONS_H

typedef enum {
    Add,
    Edit,
    View
} Action;

#endif // ACTIONS_H
