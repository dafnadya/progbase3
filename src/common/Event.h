#ifndef EVENTS_H
#define EVENTS_H

#include <QString>
#include <QDateTime>
#include <QMetaType>

class Event
{
    int id;
    QString name;
    QDate date;
    QTime time;
    double cost;
    QString details;

public:
    Event();
    Event(int id, QDate date, QTime time, double cost, QString details);

    int getId();
    QString getName();
    QDate getDate();
    QTime getTime();
    double getCost();
    QString getDetails();

    void setId(int id);
    void setName(QString name);
    void setDate(QDate date);
    void setTime(QTime time);
    void setCost(double cost);
    void setDetails(QString details);
};

Q_DECLARE_METATYPE(Event)

#endif // EVENTS_H
