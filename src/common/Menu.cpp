#include "Menu.h"

Menu::Menu(int id,
           QString name,
           double cost, double addedValue,
           QString ingrid,
           QPixmap pic,
           Category cat) {
    this->id = id;
    this->cost = cost;
    this->addedValue = addedValue;
    this->ingrid = ingrid;
    this->pic = pic;
    this->name = name;
    this->cat = cat;
}

int Menu::getId() {
    return this->id;
}

QString Menu::getName() {
    return this->name;
}

double Menu::getCost() {
    return this->cost;
}

double Menu::getAddedValue() {
    return this->addedValue;
}

QString Menu::getIngrid() {
    return this->ingrid;
}

QPixmap Menu::getPic() {
    return this->pic;
}

Category Menu::getCat() {
    return this->cat;
}

void Menu::setId(int id) {
    this->id = id;
}

void Menu::setName(QString name) {
    this->name = name;
}

void Menu::setCost(double cost) {
    this->cost = cost;
}

void Menu::setAddedValue(double value) {
    this->addedValue = value;
}

void Menu::setIngrid(QString ingrid) {
    this->ingrid = ingrid;
}

void Menu::setPic(QPixmap pic) {
    this->pic = pic;
}

void Menu::setCat(Category cat) {
    this->cat = cat;
}
