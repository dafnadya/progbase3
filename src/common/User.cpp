#include "User.h"

User::User(int id,
           QString fullName,
           PostType post,
           int salary,
           QString card,
           QString phone) {
    this->id = id;
    this->post = post;
    this->fullName = fullName;
    this->salary = salary;
    this->card = card;
    this->phone = phone;
}

int User::getId() {
    return this->id;
}

QString User::getFullName() {
    return this->fullName;
}

PostType User::getPostType() {
    return this->post;
}

int User::getSalary() {
    return this->salary;
}

QString User::getCard() {
    return this->card;
}

QString User::getPhone() {
    return this->phone;
}

void User::setId(int id) {
    this->id = id;
}

void User::setFullName(QString fullName) {
    this->fullName = fullName;
}

void User::setPostType(PostType post) {
    this->post = post;
}

void User::setSalary(int salary) {
    this->salary = salary;
}

void User::setCard(QString card) {
    this->card =  card;
}

void User::setPhone(QString phone) {
    this->phone = phone;
}

