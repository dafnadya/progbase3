#ifndef USER_H
#define USER_H

#include <QMetaType>
#include <QString>

struct LoginData {
    int user_id;
    QString login;
    QString password_hash;
};

typedef enum {
    BigBoss,
    Admin,
    Waiter,
    Barman
} PostType;

class User
{
    int id;
    QString fullName;
    PostType post;
    int salary;
    QString card;
    QString phone;
public:
    User() {}
    User(int id,
            QString fullName,
            PostType post,
            int salary,
            QString card,
            QString phone);

   int getId();
   QString getFullName();
   PostType getPostType();
   int getSalary();
   QString getCard();
   QString getPhone();

   void setId(int id);
   void setFullName(QString fullName);
   void setPostType(PostType post);
   void setSalary(int salary);
   void setCard(QString card);
   void setPhone(QString phone);
};

Q_DECLARE_METATYPE(User)

#endif // USER_H
