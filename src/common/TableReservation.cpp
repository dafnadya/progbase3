#include "TableReservation.h"

TableReservation::TableReservation()
{

}

void TableReservation::setId(int id) {
    this->id = id;
}

void TableReservation::setTable(int table) {
    this->table = table;
}

void TableReservation::setTime(QTime time) {
    this->time = time;
}

void TableReservation::setDate(QDate date) {
    this->date = date;
}

int TableReservation::getId() {
    return id;
}

int TableReservation::getTable() {
    return table;
}

QTime TableReservation::getTime() {
    return time;
}

QDate TableReservation::getDate() {
    return date;
}
