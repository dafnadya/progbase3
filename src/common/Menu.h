#ifndef MENU_H
#define MENU_H

#include <QPixmap>
#include <QMetaType>

typedef enum {
    Beer,
    Champagne,
    Wine,
    Brandy,
    Whiskey,
    Tequila,
    Cocktails,
    SoftDrinks,
    Tea,
    Coffee,
    Snacks,
    Desserts
} Category;

class Menu
{
    int id;
    QString name;
    double cost;
    double addedValue;
    QString ingrid;
    QPixmap pic;
    Category cat;

public:
    Menu() {}
    Menu(int id,
        QString name,
        double cost,
        double addedValue,
        QString ingrid,
        QPixmap pic,
        Category cat);

    int getId();
    QString getName();
    double getCost();
    double getAddedValue();
    QString getIngrid();
    QPixmap getPic();
    Category getCat();

    void setId(int id);
    void setName(QString name);
    void setCost(double cost);
    void setAddedValue(double value);
    void setIngrid(QString ingrid);
    void setPic(QPixmap pic);
    void setCat(Category cat);
};

Q_DECLARE_METATYPE(Menu)

#endif // MENU_H
