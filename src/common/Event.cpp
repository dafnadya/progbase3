#include "Event.h"

Event::Event()
{

}

Event::Event(int id, QDate date, QTime time, double cost, QString details) {
    this->id = id;
    this->time = time;
    this->date = date;
    this->cost = cost;
    this->details = details;
}

int Event::getId() {
    return this->id;
}

QString Event::getName(){
    return this->name;
}

QDate Event::getDate() {
    return this->date;
}

QTime Event::getTime() {
    return this->time;
}

double Event::getCost() {
    return this->cost;
}

QString Event::getDetails() {
    return this->details;
}

void Event::setId(int id) {
    this->id = id;
}

void Event::setName(QString name){
    this->name = name;
}

void Event::setTime(QTime time) {
    this->time = time;
}

void Event::setDate(QDate date) {
    this->date = date;
}

void Event::setCost(double cost) {
    this->cost = cost;
}

void Event::setDetails(QString details) {
    this->details = details;
}
