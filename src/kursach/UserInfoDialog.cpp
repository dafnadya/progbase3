#include "UserInfoDialog.h"
#include "ui_UserInfoDialog.h"
#include <QMessageBox>

UserInfoDialog::UserInfoDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UserInfoDialog)
{
    ui->setupUi(this);

    ui->login->hide();
    ui->password->hide();
    ui->loginEdit->hide();
    ui->passwordEdit->hide();
    ui->checkBox->hide();

    ui->login2->hide();
    ui->loginLabel->hide();
}

UserInfoDialog::~UserInfoDialog()
{
    delete ui;
}

void UserInfoDialog::setData(User user, LoginData login, Action act, User * curUser, Storage * storage) {
    stor = storage;
    if (curUser->getPostType() != BigBoss)
        ui->editButton->hide();
    else
        ui->editButton->show();
    action = act;
    if (action == View) {
        id = user.getId();
        ui->stackedWidget->setCurrentWidget(ui->viewPage);

        ui->nameLabel->setText(user.getFullName());
        ui->phoneLabel->setText(user.getPhone());
        ui->cardLabel->setText(user.getCard());
        ui->salaryLabel->setText(QString::number(user.getSalary()));
        ui->loginLabel->setText(login.login);
        PostType post = user.getPostType();
        if (post == Admin)
            ui->postLabel->setText("Admin");
        else if(post == Waiter)
            ui->postLabel->setText("Waiter");
        else if (post == Barman)
            ui->postLabel->setText("Barman");
    }
    else
        ui->stackedWidget->setCurrentWidget(ui->addEditPage);
}

void UserInfoDialog::on_loginButton_clicked(bool checked)
{
    QPixmap down(":/icons/down-arrow.png");
    QPixmap up(":/icons/up-arrow.png");
    if (checked) {
        ui->loginButton->setIcon(QIcon(up.scaled(12, 12)));
        ui->login->show();
        ui->password->show();
        ui->loginEdit->show();
        ui->passwordEdit->show();
        ui->checkBox->show();
    }
    else {
        ui->loginButton->setIcon(QIcon(down.scaled(12, 12)));
        ui->login->hide();
        ui->password->hide();
        ui->loginEdit->hide();
        ui->passwordEdit->hide();
        ui->checkBox->hide();
    }
}

void UserInfoDialog::on_login_2Button_clicked(bool checked)
{
    QPixmap down(":/icons/down-arrow.png");
    QPixmap up(":/icons/up-arrow.png");
    if (checked) {
        ui->login_2Button->setIcon(QIcon(up.scaled(12, 12)));
        ui->login2->show();
        ui->loginLabel->show();

    }
    else {
        ui->login_2Button->setIcon(QIcon(down.scaled(12, 12)));
        ui->login2->hide();
        ui->loginLabel->hide();
    }
}

void UserInfoDialog::on_okButton_clicked()
{
    close();
}

void UserInfoDialog::on_editButton_clicked()
{
    action = Edit;
    ui->stackedWidget->setCurrentWidget((ui->addEditPage));
    setWindowTitle("Edit worker");

    ui->nameEdit->setText(ui->nameLabel->text());
    ui->phoneEdit->setText(ui->phoneLabel->text());
    ui->cardEdit->setText(ui->cardLabel->text());
    ui->salaryEdit->setValue(ui->salaryLabel->text().toInt());
    ui->loginEdit->setText(ui->loginLabel->text());
    ui->password->setText("New password:");
    ui->postBox->hide();
    ui->pst->hide();
    ui->okEditButton->setEnabled(false);
}

void UserInfoDialog::on_cancelButton_clicked()
{
    close();
}

void UserInfoDialog::enableOkButton() {
    if (!ui->nameEdit->text().isEmpty() && !ui->cardEdit->text().isEmpty()
            && !ui->phoneEdit->text().isEmpty() && ui->salaryEdit->value() > 0
            && !ui->loginEdit->text().isEmpty()
            && ((action == Add && !ui->passwordEdit->text().isEmpty()) || action == Edit))
        ui->okEditButton->setEnabled(true);
    else
        ui->okEditButton->setEnabled(false);
}
void UserInfoDialog::on_nameEdit_textEdited(const QString &arg1)
{
    enableOkButton();
}

void UserInfoDialog::on_phoneEdit_textEdited(const QString &arg1)
{
    enableOkButton();
}

void UserInfoDialog::on_salaryEdit_valueChanged(int arg1)
{
    enableOkButton();
}

void UserInfoDialog::on_cardEdit_textEdited(const QString &arg1)
{
    enableOkButton();
}

void UserInfoDialog::on_loginEdit_textEdited(const QString &arg1)
{
    enableOkButton();
}

void UserInfoDialog::on_passwordEdit_textEdited(const QString &arg1)
{
    enableOkButton();
}

void UserInfoDialog::on_okEditButton_clicked()
{
    User * u = new User();
    u->setFullName(ui->nameEdit->text());
    u->setPhone(ui->phoneEdit->text());
    u->setCard(ui->cardEdit->text());
    u->setSalary(ui->salaryEdit->value());

    LoginData old = stor->getLoginDataByUserId(id);
    LoginData d;

    d.login = ui->loginEdit->text();
    QString password = ui->passwordEdit->text();
    if (!password.isEmpty()) {
        QByteArray hashArray = QCryptographicHash::hash(password.toUtf8(), QCryptographicHash::Md5);
        QString passwordHash = QString(hashArray.toHex());
        d.password_hash = passwordHash;
    }
    else
        d.password_hash = old.password_hash;

    if (action == Add) {
        u->setPostType((PostType)(ui->postBox->currentIndex() + 1));
        if (stor->isUniqueLogin(d.login)) {
            emit addUser(u, d);
            close();
        }
        else {
            QMessageBox::critical(
                        this,
                        "Error",
                        "User with this login\nalready exist",
                        QMessageBox::Ok,
                        QMessageBox::Ok);
        }
    } else if (action == Edit) {
        u->setId(id);
        d.user_id = id;
        emit editUser(u);

        if (QString::compare(d.password_hash, old.password_hash) != 0 || QString::compare(d.login, old.login) != 0) {
            if (QString::compare(d.login, old.login) == 0 || stor->isUniqueLogin(d.login)) {
                emit editLogin(d);
                close();
            }
            else {
                QMessageBox::critical(
                            this,
                            "Error",
                            "User with this login\nalready exist",
                            QMessageBox::Ok,
                            QMessageBox::Ok);
            }
        }
        else
            close();
    }
}

void UserInfoDialog::on_checkBox_clicked(bool checked)
{
    if (checked)
        ui->passwordEdit->setEchoMode(QLineEdit::Normal);
    else
        ui->passwordEdit->setEchoMode(QLineEdit::Password);
}
