#ifndef ADDMENUDIALOG_H
#define ADDMENUDIALOG_H

#include <QDialog>
#include "Menu.h"
#include "Actions.h"

#include <QFileDialog>
#include <QBuffer>

namespace Ui {
class AddMenuDialog;
}

class AddMenuDialog : public QDialog
{
    Q_OBJECT

    Menu * menu;

    Action action;

public:
    explicit AddMenuDialog(QWidget *parent = 0);
    ~AddMenuDialog();

    void setAction(Action action);

    void setMenu(Menu m);

private slots:
    void enableOkButton();

    void on_loadButton_clicked();

    void on_nameEdit_textEdited(const QString &arg1);

    void on_okButton_clicked();

    void on_cancelButton_clicked();

    void on_costEdit_valueChanged(double arg1);

    void on_categoryBox_currentIndexChanged(int index);

    void on_ingredients_textChanged();

    void on_removeButton_clicked();

    void on_valueEdit_valueChanged(double arg1);

private:
    Ui::AddMenuDialog *ui;

signals:
    void addMenu(Menu * m);
    void menuEdited(Menu * m);
};

#endif // ADDMENUDIALOG_H
