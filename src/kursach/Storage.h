#ifndef STORAGE_H
#define STORAGE_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QList>
#include <QCryptographicHash>
#include <QVariant>
#include <QBuffer>
#include "User.h"
#include "Menu.h"
#include "Event.h"
#include "TableReservation.h"
#include "Cashier.h"

class Storage : public QObject
{
    QSqlDatabase db;

    Q_OBJECT
public:
    explicit Storage(QString path, QObject *parent = nullptr);

    bool open();
    void close();

    User * userFromQuery(QSqlQuery & query);
    User * loginUser(LoginData & data);
    LoginData getLoginDataByUserId(int id);
    QList<User *> selectAllUsers();
    void addUser(User *user, LoginData log);
    void editUser(User *user);
    void editLoginData(LoginData log);
    bool isUniqueLogin(QString login);
    void removeUser(int id);
    //bool registerUser(User & data);
    Menu * menuFromQuery(QSqlQuery & query);
    QList<Menu *> selectAllMenu();
    QList<Menu *> selectCategory(Category cat);
    void addMenu(Menu *menu);
    void removeFromMenu(int id);
    void editMenu(Menu *menu);
    QList<Menu *> findMenuByName(QString name);
    QList<Menu *> findMenuByNameAndCategory(QString name, int category);
    Event * eventFromQuery(QSqlQuery & query);
    QList<Event *> selectAllEvents();
    QList<Event *> getEventsByDate(QDate date);
    void addEvent(Event *event);
    void editEvent(Event *event);
    void removeEvent(int id);
    TableReservation * reservationFromQuery(QSqlQuery & query);
    QList<TableReservation *> getReservationByTableId(int id);
    QList<TableReservation *> getReservationByDate(QDate date);
    QList<TableReservation *> getReservationByDateAndTable(QDate date, int tableId);
    QList<TableReservation *> chooseRelevantReservation(QList<TableReservation *> all);
    void removeReserv(int id);
    int addReservation(TableReservation *res);
    QList<int> getWorkScheduleByUserId(int userId);
    void removeFromWorkSchedule(int id, int day);
    void addToWorkSchedule(int id, int day);
    Cashier * cashierFromQuery(QSqlQuery & query);
    QList<Cashier *> getCashier();
    QList<Cashier *> getCashierByDate(QDate date);
    double getProfitByMonthAndYear(int month, int year);
    void addToCashier(Cashier *cash);
    int getReservationCost();
    void setReservationCost(double cost);

signals:

public slots:
};

#endif // STORAGE_H
