#ifndef CASHIERADDDIALOG_H
#define CASHIERADDDIALOG_H

#include <QDialog>
#include "User.h"
#include "Storage.h"

namespace Ui {
class CashierAddDialog;
}

class CashierAddDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CashierAddDialog(QWidget *parent = 0);
    ~CashierAddDialog();

    void setData(User * user, Storage * storage);

private slots:
    void on_lossTypeBox_currentIndexChanged(int index);

    void on_profitTypeBox_currentIndexChanged(int index);

    void on_categoryBox_currentIndexChanged(int index);

    void on_cancel3Button_clicked();

    void on_cancel2Button_clicked();

    void on_cancel1Button_clicked();

    void on_okMenuButton_clicked();

    void on_eventDateEdit_dateChanged(const QDate &date);

    void on_reserveDateEdit_dateChanged(const QDate &date);

    void on_reserveBox_currentIndexChanged(int index);

    void on_menuBox_currentIndexChanged(int index);

    void on_eventBox_currentIndexChanged(int index);

    void on_okProfitButton_clicked();

    void on_workerBox_currentIndexChanged(int index);

    void on_okLossButton_clicked();

    void on_sumEdit_valueChanged(double arg1);

    void on_targetEdit_textEdited(const QString &arg1);

    void on_tableEdit_valueChanged(int arg1);

private:
    Ui::CashierAddDialog *ui;
    Storage * stor;

signals:
    void addCash(int type, QString target, double sum);
};

#endif // CASHIERADDDIALOG_H
