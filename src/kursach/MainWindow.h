#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "User.h"
#include "AddMenuDialog.h"
#include <QAbstractButton>
#include "ReservationDialog.h"
#include "UserInfoDialog.h"
#include "CashierAddDialog.h"
#include <QPrinter>
#include <QPrintDialog>
#include "HelpDialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void insertMenuToTable(QList<Menu *> menu);

    void insertToWorkerList(QList<User *> users);

    void insertReservationToTable(QList<TableReservation *> reserv);

    void setStorage(Storage * storage);

private:
    Ui::MainWindow *ui;
    User * user;
    Storage * stor;
    QButtonGroup * tableGroup;
    QButtonGroup * categoryGroup;

public slots:
    void setUser(User * us);

    void categoryChanged(int buttonId);

    void tableSelectionChanged(int tableId);
private slots:
    void on_menuTable_itemSelectionChanged();
    void createCategoryButtonGroup();
    void createTableButtonGroup();
    void on_searchButton_clicked();
    void setEvents(const QDate &date);
    void on_calendarWidget_clicked(const QDate &date);
    void on_eventTable_doubleClicked(const QModelIndex &index);
    void on_addMenuButton_clicked();
    void addNewMenu(Menu * menu);

    void on_removeMenuButton_clicked();

    void on_editMenuButton_clicked();
    void editMenu(Menu * menu);

    void on_addEventButton_clicked();

    void eventAdd(Event * event);

    void eventEdit(Event * event);

    void setDefaultTablesColor();

    void on_allTablesButton_clicked();
    void on_eventTable_itemSelectionChanged();
    void on_removeEventButton_clicked();
    void on_cancelReservButton_clicked();
    void on_reservationTable_itemSelectionChanged();
    void on_thisTableButton_clicked();
    void on_reserveButton_clicked();
    void addNewReservation(TableReservation * res);
    void on_reservationTable_doubleClicked(const QModelIndex &index);

    void insertToShiftTable();

    void on_uncheckedButton_clicked();

    void on_checkedButton_clicked();

    void on_shiftTable_itemSelectionChanged();

    void on_workerList_doubleClicked(const QModelIndex &index);

    void userEdit(User * us);
    void loginDataEdit(LoginData log);
    void userAdd(User * us, LoginData log);

    void on_addWorkerButton_clicked();

    void on_removeWorkerButton_clicked();

    void on_workerList_itemSelectionChanged();

    void on_profitButton_clicked();

    void on_lossButton_clicked();

    void cashierAdd(int type, QString target, double sum);

    void insertCashierToTable(QList<Cashier *> cash);

    void on_dateCashEdit_dateChanged(const QDate &date);

    void createPlot(int month, int year);

    void on_yearBox_valueChanged(int arg1);

    void on_helpButton_clicked();

    void on_exitButton_clicked();

    void on_printButton_clicked();

signals:
    void logout();
};

#endif // MAINWINDOW_H
