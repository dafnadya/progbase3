#include "EventDetailsDialog.h"
#include "ui_EventDetailsDialog.h"

EventDetailsDialog::EventDetailsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EventDetailsDialog)
{
    ui->setupUi(this);
}

EventDetailsDialog::~EventDetailsDialog()
{
    delete ui;
}

void EventDetailsDialog::setData(Event ev, Action act, User * user) {
    if (user->getPostType() != 1)
        ui->editButton->hide();
    else
        ui->editButton->show();
    action = act;
    if (action == View) {
        id = ev.getId();
        ui->stackedWidget->setCurrentWidget(ui->viewPage);
        ui->nameLabel->setText(ev.getName());
        ui->timeLabel->setText(ev.getTime().toString("hh:mm"));
        double cost = ev.getCost();
        if (cost == 0)
            ui->costLabel->setText("free");
        else
           ui->costLabel->setText(QString::number(cost) + " $");
        ui->detailsLabel->setText(ev.getDetails());
    }
    else
        ui->stackedWidget->setCurrentWidget(ui->editAddPage);
}

void EventDetailsDialog::on_okButton_clicked()
{
    close();
}

void EventDetailsDialog::on_editButton_clicked()
{
    action = Edit;
    ui->stackedWidget->setCurrentWidget(ui->editAddPage);
    setWindowTitle("Edit event");
    ui->nameEdit->setText(ui->nameLabel->text());
    ui->timeEdit->setTime(QTime::fromString(ui->timeLabel->text()));
    ui->costEdit->setValue(ui->costLabel->text().toDouble());
    ui->detailsEdit->appendPlainText(ui->detailsLabel->toPlainText());
    ui->okEditButton->setEnabled(false);
}

void EventDetailsDialog::on_okEditButton_clicked()
{
    Event * ev = new Event();
    ev->setName(ui->nameEdit->text());
    ev->setCost(ui->costEdit->value());
    ev->setTime(ui->timeEdit->time());
    ev->setDetails(ui->detailsEdit->toPlainText());
    if (action == Add)
        emit addEvent(ev);
    else if (action == Edit) {
        ev->setId(id);
        emit editEvent(ev);
    }
    close();
}

void EventDetailsDialog::on_cancelButton_clicked()
{
    close();
}

void EventDetailsDialog::enableOkButton() {
    if (!ui->nameEdit->text().isEmpty())
        ui->okEditButton->setEnabled(true);
    else
        ui->okEditButton->setEnabled(false);
}

void EventDetailsDialog::on_nameEdit_textEdited(const QString &arg1)
{
    enableOkButton();
}

void EventDetailsDialog::on_timeEdit_timeChanged(const QTime &time)
{
    enableOkButton();
}

void EventDetailsDialog::on_costEdit_valueChanged(double arg1)
{
    enableOkButton();
}

void EventDetailsDialog::on_detailsEdit_textChanged()
{
    enableOkButton();
}
