#ifndef RESERVATIONDIALOG_H
#define RESERVATIONDIALOG_H

#include <QDialog>

#include "TableReservation.h"

namespace Ui {
class ReservationDialog;
}

class ReservationDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ReservationDialog(QWidget *parent = 0);
    ~ReservationDialog();

    void setTableNumber(int id);

private slots:
    void on_buttonBox_accepted();

    void on_dateEdit_dateChanged(const QDate &date);

private:
    Ui::ReservationDialog *ui;
signals:
    void reservedTable(TableReservation * reserv);
};

#endif // RESERVATIONDIALOG_H
