#ifndef USERINFODIALOG_H
#define USERINFODIALOG_H

#include <QDialog>
#include "Actions.h"
#include "User.h"
#include "Authorization.h"
#include "Storage.h"

namespace Ui {
class UserInfoDialog;
}

class UserInfoDialog : public QDialog
{
    Q_OBJECT

    int id;

    Storage * stor;

    Action action;

public:
    explicit UserInfoDialog(QWidget *parent = 0);
    ~UserInfoDialog();

    void setData(User user, LoginData login, Action act, User * curUser, Storage *storage);

private slots:
    void on_loginButton_clicked(bool checked);

    void on_login_2Button_clicked(bool checked);

    void on_okButton_clicked();

    void on_editButton_clicked();

    void on_cancelButton_clicked();

    void enableOkButton();

    void on_nameEdit_textEdited(const QString &arg1);

    void on_phoneEdit_textEdited(const QString &arg1);

    void on_salaryEdit_valueChanged(int arg1);

    void on_cardEdit_textEdited(const QString &arg1);

    void on_loginEdit_textEdited(const QString &arg1);

    void on_passwordEdit_textEdited(const QString &arg1);

    void on_okEditButton_clicked();

    void on_checkBox_clicked(bool checked);

private:
    Ui::UserInfoDialog *ui;
signals:
    void addUser(User * ev, LoginData l);
    void editUser(User * ev);
    void editLogin(LoginData l);
};

#endif // USERINFODIALOG_H
