#include "Storage.h"

using namespace std;

Storage::Storage(QString path, QObject *parent) : QObject(parent)
{
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(path);
}

User * Storage::userFromQuery(QSqlQuery & query) {
    User * s = new User();
    s->setId(query.value("id").toInt());
    s->setFullName(query.value("fullName").toString());
    int post = query.value("postId").toInt();
    s->setPostType((PostType)(post - 1));
    s->setSalary(query.value("salary").toInt());
    s->setCard(query.value("card").toString());
    s->setPhone(query.value("phone").toString());
    return s;
}

QList<User *> Storage::selectAllUsers() {
    QList<User *> users;
    QSqlQuery query("SELECT * FROM users");
    if (!query.exec())
        throw query.lastError();
    while(query.next()) {
        User * s = userFromQuery(query);
        users.push_back(s);
    }
    return users;
}

User * Storage::loginUser(LoginData & data) {
    QSqlQuery query;
    query.prepare("SELECT * FROM login WHERE username = :login AND password_hash = :hash");
    query.bindValue(":login", data.login);
    QByteArray hashArray = QCryptographicHash::hash(data.password_hash.toUtf8(), QCryptographicHash::Md5);
    QString passwordHash = QString(hashArray.toHex());
    query.bindValue(":hash", passwordHash);
    if (!query.exec()) {
        throw query.lastError();
    }
    if (query.next()) {
        int user_id = query.value("user_id").toInt();
        query.prepare("SELECT * FROM users WHERE id = :user_id");
        query.bindValue(":user_id", user_id);
        if (!query.exec()) {
            throw query.lastError();
        }
        if (query.next()) {
           return userFromQuery(query);
        }
    }
    return nullptr;
}

LoginData Storage::getLoginDataByUserId(int id) {
    LoginData data;
    QSqlQuery query;
    query.prepare("SELECT * FROM login WHERE user_id = :id");
    query.bindValue(":id", id);
    if (!query.exec()) {
        throw query.lastError();
    }
    if (query.next()) {
        data.user_id = query.value("user_id").toInt();
        data.login = query.value("username").toString();
        data.password_hash = query.value("password_hash").toString();
    }
    return data;
}


void Storage::addUser(User *user, LoginData log) {
        QSqlQuery query;
        query.prepare("INSERT INTO users (fullName, postId, phone, salary, card) VALUES (:name, :post, :phone, :sal, :card)");
        query.bindValue(":name", user->getFullName());
        query.bindValue(":post", (int)user->getPostType() + 1);
        query.bindValue(":phone", user->getPhone());
        query.bindValue(":sal", user->getSalary());
        query.bindValue(":card", user->getCard());
        if (!query.exec()) {
            throw query.lastError();
        }
        int user_id = query.lastInsertId().toInt();
        query.clear();
        query.prepare("INSERT INTO login (username, password_hash, user_id) VALUES (:login, :hash, :user_id)");
        query.bindValue(":login", log.login);
        query.bindValue(":user_id", user_id);
        query.bindValue(":hash", log.password_hash);
        if (!query.exec()) {
            throw query.lastError();
        }
}

void Storage::removeUser(int id) {
    QSqlQuery query;
    query.prepare("DELETE FROM users WHERE id = :id");
    query.bindValue(":id", id);
    if (!query.exec()) {
        throw query.lastError();
    }
    query.clear();
    query.prepare("DELETE FROM login WHERE user_id = :id");
    query.bindValue(":id", id);
    if (!query.exec()) {
        throw query.lastError();
    }
}

void Storage::editUser(User *user) {
    int id = user->getId();
    QSqlQuery query;
    query.prepare("UPDATE users SET fullName = :name, phone = :phone, salary = :salary,"
                  " card = :card WHERE id = :id");
    query.bindValue(":id", id);
    query.bindValue(":name", user->getFullName());
    query.bindValue(":phone", user->getPhone());
    query.bindValue(":salary", user->getSalary());
    query.bindValue(":card", user->getCard());
    if(!query.exec())
        throw query.lastError();
}

void Storage::editLoginData(LoginData log) {
    int user_id = log.user_id;
    QSqlQuery query;
    query.prepare("UPDATE login SET username = :name, password_hash = :password WHERE user_id = :id");
    query.bindValue(":id", user_id);
    query.bindValue(":name", log.login);
    query.bindValue(":password", log.password_hash);
    if(!query.exec())
        throw query.lastError();
}

bool Storage::isUniqueLogin(QString login) {
    QSqlQuery query;
    query.prepare("SELECT * FROM login WHERE username = :login");
    query.bindValue(":login", login);
    if (!query.exec()) {
        throw query.lastError();
    }
    if(query.next()) {
        return false;
    }
    return true;
}

Menu * Storage::menuFromQuery(QSqlQuery & query) {
    Menu * s = new Menu();
    s->setId(query.value("id").toInt());
    s->setName(query.value("name").toString());
    s->setCost(query.value("cost").toDouble());
    s->setAddedValue(query.value("added_value").toDouble());
    s->setIngrid(query.value("ingredients").toString());
    QByteArray outByteArray = query.value("pic").toByteArray();
    QPixmap pic = QPixmap();
    pic.loadFromData(outByteArray);
    s->setPic(pic);
    int categ = query.value("category_id").toInt();
    s->setCat((Category)(categ - 1));
    return s;
}

QList<Menu *> Storage::selectAllMenu() {
    QList<Menu *> menu;
    QSqlQuery query("SELECT * FROM menu");
    if (!query.exec())
        throw query.lastError();
    while(query.next()) {
        Menu * s = menuFromQuery(query);
        menu.push_back(s);
    }
    return menu;
}

QList<Menu *> Storage::selectCategory(Category cat) {
    int id = (int)cat + 1;
    QList<Menu *> menu;
    QSqlQuery query;
    query.prepare("SELECT * FROM menu WHERE category_id = :id");
    query.bindValue(":id", id);
    if (!query.exec()) {
        throw query.lastError();
    }
    while(query.next()) {
        Menu * s = menuFromQuery(query);
        menu.push_back(s);
    }
    return menu;
}

void Storage::addMenu(Menu *menu) {
    QSqlQuery query;
    query.prepare("INSERT INTO menu (name, cost, added_value, ingredients, pic, category_id) "
                  "VALUES (:name, :cost, :value, :ing, :pic, :cat)");
    query.bindValue(":name", menu->getName());
    query.bindValue(":cost", menu->getCost());
    query.bindValue(":value", menu->getAddedValue());
    query.bindValue(":ing", menu->getIngrid());
    query.bindValue(":cat", (int)menu->getCat() + 1);
    QPixmap pic = menu->getPic();
    QByteArray inByteArray;
    QBuffer inBuffer(&inByteArray);
    inBuffer.open(QIODevice::WriteOnly);
    pic.save(&inBuffer, "PNG");
    query.bindValue(":pic", inByteArray);
    if(!query.exec())
        throw query.lastError();
}

void Storage::removeFromMenu(int id) {
    QSqlQuery query;
    query.prepare("DELETE FROM menu WHERE id = :id");
    query.bindValue(":id", id);
    if (!query.exec()) {
        throw query.lastError();
    }
}

void Storage::editMenu(Menu *menu) {
    int id = menu->getId();
    QSqlQuery query;
    query.prepare("UPDATE menu SET name = :name, cost = :cost, added_value = :value, ingredients = :ing,"
                  " pic = :pic, category_id = :cat WHERE id = :id");
    query.bindValue(":id", id);
    query.bindValue(":name", menu->getName());
    query.bindValue(":cost", menu->getCost());
    query.bindValue(":value", menu->getAddedValue());
    query.bindValue(":ing", menu->getIngrid());
    query.bindValue(":cat", (int)menu->getCat() + 1);
    QPixmap pic = menu->getPic();
    QByteArray inByteArray;
    QBuffer inBuffer(&inByteArray);
    inBuffer.open(QIODevice::WriteOnly);
    pic.save(&inBuffer, "PNG");
    query.bindValue(":pic", inByteArray);
    if(!query.exec())
        throw query.lastError();
}

QList<Menu *> Storage::findMenuByName(QString name) {
    QList<Menu *> menu;
    QSqlQuery query;
    QString search = "%" + name + "%";
    query.prepare("SELECT * FROM menu WHERE name LIKE :search");
    query.bindValue(":search", search);
    if (!query.exec()) {
        throw query.lastError();
    }
    while(query.next()) {
        Menu * s = menuFromQuery(query);
        menu.push_back(s);
    }
    return menu;
}

QList<Menu *> Storage::findMenuByNameAndCategory(QString name, int category) {
    QList<Menu *> menu;
    QSqlQuery query;
    QString search = "%" + name + "%";
    query.prepare("SELECT * FROM menu WHERE name LIKE :search AND category_id = :cat");
    query.bindValue(":search", search);
    query.bindValue(":cat", category);
    if (!query.exec()) {
        throw query.lastError();
    }
    while(query.next()) {
        Menu * s = menuFromQuery(query);
        menu.push_back(s);
    }
    return menu;
}

Event * Storage::eventFromQuery(QSqlQuery & query) {
    Event * s = new Event();
    s->setId(query.value("id").toInt());
    s->setName(query.value("name").toString());
    s->setTime(QTime::fromString(query.value("time").toString(), Qt::ISODate));
    s->setDate(QDate::fromString(query.value("date").toString(), Qt::ISODate));
    s->setCost(query.value("cost").toDouble());
    s->setDetails(query.value("details").toString());
    return s;
}

QList<Event *> Storage::selectAllEvents() {
    QList<Event *> events;
    QSqlQuery query("SELECT * FROM events");
    if (!query.exec())
        throw query.lastError();
    while(query.next()) {
        Event * s = eventFromQuery(query);
        events.push_back(s);
    }
    return events;
}

QList<Event *> Storage::getEventsByDate(QDate date) {
    QList<Event *> events;
    QSqlQuery query;
    query.prepare("select * from events where date = :date");
    query.bindValue(":date", date.toString(Qt::ISODate));
    if (!query.exec()) {
        throw query.lastError();
    }
    while(query.next()) {
        Event * s = eventFromQuery(query);
        events.push_back(s);
    }
    return events;
}

void Storage::addEvent(Event *event) {
    QSqlQuery query;
    query.prepare("INSERT INTO events (name, date, time, cost, details) "
                  "VALUES (:name, :date, :time, :cost, :details)");
    query.bindValue(":name", event->getName());
    query.bindValue(":date", event->getDate());
    query.bindValue(":time", event->getTime().toString(Qt::ISODate));
    query.bindValue(":cost", event->getCost());
    query.bindValue(":details", event->getDetails());
    if(!query.exec())
        throw query.lastError();
}

void Storage::editEvent(Event *event) {
    int id = event->getId();
    QSqlQuery query;
    query.prepare("UPDATE events SET name = :name, cost = :cost, date = :date,"
                  " time = :time, details = :details WHERE id = :id");
    query.bindValue(":id", id);
    query.bindValue(":name", event->getName());
    query.bindValue(":cost", event->getCost());
    query.bindValue(":date", event->getDate().toString(Qt::ISODate));
    query.bindValue(":time", event->getTime());
    query.bindValue(":details", event->getDetails());
    if(!query.exec())
        throw query.lastError();
}

void Storage::removeEvent(int id) {
    QSqlQuery query;
    query.prepare("DELETE FROM events WHERE id = :id");
    query.bindValue(":id", id);
    if (!query.exec()) {
        throw query.lastError();
    }
}

TableReservation * Storage::reservationFromQuery(QSqlQuery & query) {
    TableReservation * s = new TableReservation();
    s->setId(query.value("id").toInt());
    s->setTable(query.value("table_id").toInt());
    s->setTime(QTime::fromString(query.value("time").toString(), Qt::ISODate));
    s->setDate(QDate::fromString(query.value("date").toString(), Qt::ISODate));
    return s;
}

QList<TableReservation *> Storage::chooseRelevantReservation(QList<TableReservation *> all) {
    QDate curDate = QDate::currentDate();
    QList<TableReservation *> reserv;
    for(TableReservation * r : all) {
        if (r->getDate() >= curDate)
            reserv.push_back(r);
    }
    return reserv;
}

QList<TableReservation *> Storage::getReservationByTableId(int id) {
    QList<TableReservation *> reserv;
    QSqlQuery query;
    query.prepare("SELECT * FROM tableReservations WHERE table_id = :id");
    query.bindValue(":id", id);
    if (!query.exec()) {
        throw query.lastError();
    }
    while(query.next()) {
        TableReservation * s = reservationFromQuery(query);
        reserv.push_back(s);
    }
    return chooseRelevantReservation(reserv);
}

QList<TableReservation *> Storage::getReservationByDate(QDate date) {
    QList<TableReservation *> reserv;
    QSqlQuery query;
    query.prepare("SELECT * FROM tableReservations WHERE date = :date");
    query.bindValue(":date", date.toString(Qt::ISODate));
    if (!query.exec()) {
        throw query.lastError();
    }
    while(query.next()) {
        TableReservation * s = reservationFromQuery(query);
        reserv.push_back(s);
    }
    return reserv;
}

QList<TableReservation *> Storage::getReservationByDateAndTable(QDate date, int tableId) {
    QList<TableReservation *> reserv = getReservationByDate(date);
    QList<TableReservation *> res;
    for(TableReservation * r : reserv) {
        if (r->getTable() == tableId)
            res.push_back(r);
    }
    return res;
}

int Storage::addReservation(TableReservation *res) {
    QSqlQuery query;
    query.prepare("INSERT INTO tableReservations (time, date, table_id) "
                  "VALUES (:time, :date, :table_id)");
    query.bindValue(":time", res->getTime().toString(Qt::ISODate));
    query.bindValue(":date", res->getDate().toString(Qt::ISODate));
    query.bindValue(":table_id", res->getTable());
    if(!query.exec())
        throw query.lastError();
    QVariant var = query.lastInsertId();
    return var.value<int>();
}

void Storage::removeReserv(int id) {
    QSqlQuery query;
    query.prepare("DELETE FROM tableReservations WHERE id = :id");
    query.bindValue(":id", id);
    if (!query.exec()) {
        throw query.lastError();
    }
}

QList<int> Storage::getWorkScheduleByUserId(int userId) {
    QSqlQuery query;
    query.prepare("SELECT * FROM workSchedule WHERE user_id = :userId");
    query.bindValue(":userId", userId);
    if (!query.exec()) {
        throw query.lastError();
    }
    QList<int> list;
    while (query.next()) {
        int day = query.value("dayOfWeek").toInt();
        list.push_back(day);
    }
    return list;
}

void Storage::removeFromWorkSchedule(int id, int day) {
    QSqlQuery query;
    query.prepare("DELETE FROM workSchedule WHERE user_id = :id AND dayOfWeek = :day");
    query.bindValue(":id", id);
    query.bindValue(":day", day);
    if (!query.exec()) {
        throw query.lastError();
    }
}

void Storage::addToWorkSchedule(int id, int day) {
    QSqlQuery query;
    query.prepare("INSERT INTO workSchedule (user_id, dayOfWeek) "
                  "VALUES (:id, :day)");
    query.bindValue(":id", id);
    query.bindValue(":day", day);
    if(!query.exec())
        throw query.lastError();
}

Cashier * Storage::cashierFromQuery(QSqlQuery & query) {
    Cashier * c = new Cashier();
    c->setType(query.value("type").toInt());
    c->setTarget(query.value("target").toString());
    c->setSum(query.value("sum").toDouble());
    c->setDate(QDate::fromString(query.value("date").toString(), Qt::ISODate));
    return c;
}

QList<Cashier *> Storage::getCashier() {
    QList<Cashier *> cash;
    QSqlQuery query("SELECT * FROM cashier");
    if (!query.exec())
        throw query.lastError();
    while(query.next()) {
        Cashier * c = cashierFromQuery(query);
        cash.push_back(c);
    }
    return cash;
}

QList<Cashier *> Storage::getCashierByDate(QDate date) {
    QList<Cashier *> cash;
    QSqlQuery query;
    query.prepare("SELECT * FROM cashier WHERE date = :date");
    query.bindValue(":date", date.toString(Qt::ISODate));
    if (!query.exec())
        throw query.lastError();
    while(query.next()) {
        Cashier * c = cashierFromQuery(query);
        cash.push_back(c);
    }
    return cash;
}

void Storage::addToCashier(Cashier *cash) {
        QSqlQuery query;
        query.prepare("INSERT INTO cashier (type, sum, target, date) VALUES (:type, :sum, :target, :date)");
        query.bindValue(":type", cash->getType());
        query.bindValue(":sum", cash->getSum());
        query.bindValue(":target", cash->getTarget());
        query.bindValue(":date", cash->getDate().toString(Qt::ISODate));
        if (!query.exec()) {
            throw query.lastError();
        }
}

double Storage::getProfitByMonthAndYear(int month, int year) {
    QSqlQuery query;
    QString search;
    if (month < 10)
        search = "%" + QString::number(year) + "-0" + QString::number(month) + "%";
    else
        search = "%" + QString::number(year) + "-" + QString::number(month) + "%";
    query.prepare("SELECT SUM(sum) FROM cashier WHERE type = 0 AND date LIKE :search");
    query.bindValue(":search", search);
    if (!query.exec()) {
        throw query.lastError();
    }
    double profit = 0;
    if (query.next()) {
        profit = query.value("SUM(sum)").toDouble();
    }

    query.clear();
    query.prepare("SELECT SUM(sum) FROM cashier WHERE type = 1 AND date LIKE :search");
    query.bindValue(":search", search);
    if (!query.exec()) {
        throw query.lastError();
    }
    double loss = 0;
    if (query.next()) {
        loss = query.value("SUM(sum)").toDouble();
    }
    return profit - loss;
}

int Storage::getReservationCost() {
    int cost = 0;
    QSqlQuery query("SELECT * FROM reservationCost");
    if (!query.exec())
        throw query.lastError();
    else if(query.next()) {
        cost = query.value("cost").toInt();
    }
    return cost;
}

void Storage::setReservationCost(double cost) {
    QSqlQuery query;
    query.prepare("UPDATE reservationCost SET cost = :cost WHERE id = :id");
    query.bindValue(":id", 1);
    query.bindValue(":cost", cost);
    if(!query.exec())
        throw query.lastError();
}

bool Storage::open() {
    return db.open();
}

void Storage::close() {
    db.close();
}
