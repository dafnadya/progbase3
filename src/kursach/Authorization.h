#ifndef AUTHORIZATION_H
#define AUTHORIZATION_H

#include <QMainWindow>
#include <QtSql>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include "Storage.h"
#include "MainWindow.h"
class MainWindow;

namespace Ui {
class Authorization;
}

class Authorization : public QMainWindow
{
    Q_OBJECT

public:
    explicit Authorization(QWidget *parent = 0);
    ~Authorization();

private:
    Ui::Authorization *ui;

public:
    Storage * stor;

signals:
    void loginChanged(bool entered);

private slots:
    void on_show_toggled(bool checked);
    void on_login_textEdited(const QString &arg1);
    void on_password_textEdited(const QString &arg1);
    void on_loginButton_clicked();

private:
    MainWindow *wind;
};

#endif // AUTHORIZATION_H
