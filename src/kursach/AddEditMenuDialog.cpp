#include "AddMenuDialog.h"
#include "ui_AddMenuDialog.h"

AddMenuDialog::AddMenuDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddMenuDialog)
{
    ui->setupUi(this);

    menu = new Menu();
    QPixmap p(":/icons/nophoto.png");
    menu->setPic(p);
    ui->valueEdit->setMaximum(ui->costEdit->value());
}

AddMenuDialog::~AddMenuDialog()
{
    delete ui;
}

void AddMenuDialog::setAction(Action action){
    this->action = action;
    if (action == Edit)
        ui->removeButton->setEnabled(true);
}

void AddMenuDialog::setMenu(Menu m) {
    menu->setId(m.getId());
    menu->setPic(m.getPic());
    ui->nameEdit->setText(m.getName());

    int w = ui->picLabel->width();
    int h = ui->picLabel->height();
    ui->picLabel->setPixmap(m.getPic().scaled(w,h,Qt::KeepAspectRatio));

    ui->ingredients->appendPlainText(m.getIngrid());
    ui->costEdit->setValue(m.getCost());
    ui->valueEdit->setValue(m.getAddedValue());
    ui->valueEdit->setMaximum(m.getCost());
    ui->categoryBox->setCurrentIndex((int)m.getCat());

    ui->okButton->setEnabled(false);
}

void AddMenuDialog::enableOkButton() {
    if(!ui->nameEdit->text().isEmpty() && ui->costEdit->value() > 0 && ui->valueEdit->value() > 0) {
        ui->okButton->setEnabled(true);
    }
    else
        ui->okButton->setEnabled(false);
}

void AddMenuDialog::on_loadButton_clicked()
{
    enableOkButton();
    QString path = QFileDialog::getOpenFileName(
                    this,
                    tr("Open image"),
                    "",
                    tr("IMAGES (*.jpg *.png)"));
    if (!path.isEmpty()) {
        ui->removeButton->setEnabled(true);
        QPixmap p(path);
        menu->setPic(p);
        int w = ui->picLabel->width();
        int h = ui->picLabel->height();
        ui->picLabel->setPixmap(p.scaled(w,h,Qt::KeepAspectRatio));
    }
}

void AddMenuDialog::on_nameEdit_textEdited(const QString &arg1)
{
    enableOkButton();
}

void AddMenuDialog::on_costEdit_valueChanged(double arg1)
{
    ui->valueEdit->setMaximum(arg1);
    enableOkButton();
}

void AddMenuDialog::on_categoryBox_currentIndexChanged(int index)
{
    enableOkButton();
}

void AddMenuDialog::on_ingredients_textChanged()
{
    enableOkButton();
}

void AddMenuDialog::on_okButton_clicked()
{
    menu->setName(ui->nameEdit->text());
    menu->setIngrid(ui->ingredients->toPlainText());
    menu->setCost(ui->costEdit->value());
    menu->setAddedValue(ui->valueEdit->value());
    menu->setCat((Category)ui->categoryBox->currentIndex());
    if (action == Add)
        emit addMenu(menu);
    else if (action == Edit)
        emit menuEdited(menu);
    close();
}

void AddMenuDialog::on_cancelButton_clicked()
{
    close();
}

void AddMenuDialog::on_removeButton_clicked()
{
    enableOkButton();
    QPixmap p(":/icons/nophoto.png");
    menu->setPic(p);
    ui->picLabel->setPixmap(p);
}

void AddMenuDialog::on_valueEdit_valueChanged(double arg1)
{
    enableOkButton();
}
