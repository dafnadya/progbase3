#include "CashierAddDialog.h"
#include "ui_CashierAddDialog.h"

CashierAddDialog::CashierAddDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CashierAddDialog)
{
    ui->setupUi(this);
    ui->eventDateEdit->setDate(QDate::currentDate());
    ui->reserveDateEdit->setDate(QDate::currentDate());
}

CashierAddDialog::~CashierAddDialog()
{
    delete ui;
}

void CashierAddDialog::setData(User * user, Storage *storage) {
    stor = storage;
    if (user->getPostType() == BigBoss) {
        ui->stackedWidget->setCurrentWidget(ui->lossPage);
        ui->sumLbl->hide();
        ui->sumEdit->hide();
        ui->targetLbl->hide();
        ui->targetEdit->hide();

        ui->workerBox->clear();
        QList<User *> users = stor->selectAllUsers();
        for (User * m : users) {
            QVariant var;
            var.setValue(*m);
            ui->workerBox->addItem(m->getFullName(), var);
        }
    }
    else if (user->getPostType() == Admin) {
        ui->stackedWidget->setCurrentWidget(ui->elseProfitPage);
        ui->tableLbl->hide();
        ui->tableEdit->hide();
        ui->reserveDateLbl->hide();
        ui->reserveDateEdit->hide();
        ui->reserveLbl->hide();
        ui->reserveBox->hide();

        ui->eventBox->clear();
        QList<Event *> events = stor->getEventsByDate(ui->eventDateEdit->date());
        for (Event * m : events) {
            QVariant var;
            var.setValue(*m);
            ui->eventBox->addItem(m->getName(), var);
        }

        ui->reserveBox->clear();
        QList<TableReservation *> reserves = stor->getReservationByDate(ui->reserveDateEdit->date());
        for (TableReservation * m : reserves) {
            QVariant var;
            var.setValue(*m);
            ui->reserveBox->addItem(m->getTime().toString(Qt::ISODate), var);
        }
    }
    else {
        ui->stackedWidget->setCurrentWidget(ui->menuProfitPage);
        ui->menuBox->clear();
        QList<Menu *> menu = stor->selectAllMenu();
        for (Menu * m : menu) {
            QVariant var;
            var.setValue(*m);
            ui->menuBox->addItem(m->getName(), var);
        }
    }
}

void CashierAddDialog::on_lossTypeBox_currentIndexChanged(int index)
{
    ui->workerLbl->hide();
    ui->workerBox->hide();
    ui->sumLbl->hide();
    ui->sumEdit->hide();
    ui->targetLbl->hide();
    ui->targetEdit->hide();

    if (index == 0) { // salary
        ui->workerLbl->show();
        ui->workerBox->show();
        emit ui->workerBox->currentIndexChanged(ui->workerBox->currentIndex());
    }
    else if (index == 1) { // taxes
        ui->sumLbl->show();
        ui->sumEdit->show();
        emit ui->sumEdit->valueChanged(ui->sumEdit->value());
    }
    else { // else
        ui->targetLbl->show();
        ui->targetEdit->show();
        ui->sumLbl->show();
        ui->sumEdit->show();
        emit ui->targetEdit->textEdited(ui->targetEdit->text());
    }
}

void CashierAddDialog::on_profitTypeBox_currentIndexChanged(int index)
{
    ui->eventLbl->hide();
    ui->eventBox->hide();
    ui->eventDateLbl->hide();
    ui->eventDateEdit->hide();
    ui->tableLbl->hide();
    ui->tableEdit->hide();
    ui->reserveDateEdit->hide();
    ui->reserveDateEdit->hide();
    ui->reserveLbl->hide();
    ui->reserveBox->hide();
    if (index == 0) { // event
        ui->eventLbl->show();
        ui->eventBox->show();
        ui->eventDateLbl->show();
        ui->eventDateEdit->show();
        emit ui->eventBox->currentIndexChanged(ui->eventBox->currentIndex());
    }
    else { // table reservation
        ui->tableLbl->show();
        ui->tableEdit->show();
        ui->reserveDateLbl->show();
        ui->reserveDateEdit->show();
        ui->reserveLbl->show();
        ui->reserveBox->show();
        emit ui->reserveBox->currentIndexChanged(ui->reserveBox->currentIndex());
    }
}

void CashierAddDialog::on_categoryBox_currentIndexChanged(int index)
{
    ui->menuBox->clear();
    QList<Menu *> menu;
    if (index == 0) {
        menu = stor->selectAllMenu();
    }
    else
        menu = stor->selectCategory((Category)(index - 1));
    for (Menu * m : menu) {
        QVariant var;
        var.setValue(*m);
        ui->menuBox->addItem(m->getName(), var);
    }
}

void CashierAddDialog::on_cancel3Button_clicked()
{
    close();
}

void CashierAddDialog::on_cancel2Button_clicked()
{
    close();
}

void CashierAddDialog::on_cancel1Button_clicked()
{
    close();
}

void CashierAddDialog::on_okMenuButton_clicked()
{
    int index = ui->menuBox->currentIndex();
    QVariant var = ui->menuBox->itemData(index);
    Menu m = var.value<Menu>();
    QString target = "Order: " + m.getName();
    emit addCash(0, target, m.getAddedValue());
    close();
}

void CashierAddDialog::on_eventDateEdit_dateChanged(const QDate &date)
{
    ui->eventBox->clear();
    QList<Event *> events = stor->getEventsByDate(date);
    for (Event * m : events) {
        QVariant var;
        var.setValue(*m);
        ui->eventBox->addItem(m->getName(), var);
    }
}

void CashierAddDialog::on_reserveDateEdit_dateChanged(const QDate &date)
{
    ui->reserveBox->clear();
    QList<TableReservation *> reserves = stor->getReservationByDateAndTable(date, ui->tableEdit->value());
    for (TableReservation * m : reserves) {
        QVariant var;
        var.setValue(*m);
        ui->reserveBox->addItem(m->getTime().toString(Qt::ISODate), var);
    }
}

void CashierAddDialog::on_reserveBox_currentIndexChanged(int index)
{
    if (ui->reserveBox->count() == 0)
        ui->okProfitButton->setEnabled(false);
    else
        ui->okProfitButton->setEnabled(true);
}

void CashierAddDialog::on_menuBox_currentIndexChanged(int index)
{
    if (ui->menuBox->count() == 0)
        ui->okMenuButton->setEnabled(false);
    else
        ui->okMenuButton->setEnabled(true);
}

void CashierAddDialog::on_eventBox_currentIndexChanged(int index)
{
    if (ui->eventBox->count() == 0)
        ui->okProfitButton->setEnabled(false);
    else
        ui->okProfitButton->setEnabled(true);
}

void CashierAddDialog::on_okProfitButton_clicked()
{
    int option = ui->profitTypeBox->currentIndex();
    QString target;
    double cost;
    if (option == 0) {
        int index = ui->eventBox->currentIndex();
        QVariant var = ui->eventBox->itemData(index);
        Event e = var.value<Event>();
        target = "Event: " + e.getName() + " on " + e.getDate().toString(Qt::ISODate);
        cost = e.getCost();
    }
    else {
        int index = ui->reserveBox->currentIndex();
        QVariant var = ui->reserveBox->itemData(index);
        TableReservation t = var.value<TableReservation>();
        target = "Table number " + QString::number(t.getTable()) + " reservation on " +
                t.getDate().toString(Qt::ISODate) + " " + t.getTime().toString("hh:mm");
        cost = stor->getReservationCost();
    }
    emit addCash(0, target, cost);
    close();
}

void CashierAddDialog::on_workerBox_currentIndexChanged(int index)
{
    if (ui->workerBox->count() == 0)
        ui->okLossButton->setEnabled(false);
    else
        ui->okLossButton->setEnabled(true);
}

void CashierAddDialog::on_okLossButton_clicked()
{
    int option = ui->lossTypeBox->currentIndex();
    QString target;
    double cost;
    if (option == 0) { // salary
        int index = ui->workerBox->currentIndex();
        QVariant var = ui->workerBox->itemData(index);
        User u = var.value<User>();
        target = "Salary to " + u.getFullName();
        cost = u.getSalary();
    }
    else if (option == 1) { // taxes
        cost = ui->sumEdit->value();
        target = "Taxes";
    }
    else {
        cost = ui->sumEdit->value();
        target = ui->targetEdit->text();
    }
    emit addCash(1, target, cost);
    close();
}

void CashierAddDialog::on_sumEdit_valueChanged(double arg1)
{
    if (arg1 > 0)
        ui->okLossButton->setEnabled(true);
    else
        ui->okLossButton->setEnabled(false);
}

void CashierAddDialog::on_targetEdit_textEdited(const QString &arg1)
{
    if (!arg1.isEmpty() && ui->sumEdit->value() > 0)
        ui->okLossButton->setEnabled(true);
    else
        ui->okLossButton->setEnabled(false);
}

void CashierAddDialog::on_tableEdit_valueChanged(int arg1)
{
    ui->reserveBox->clear();
    QList<TableReservation *> reserves = stor->getReservationByDateAndTable(ui->reserveDateEdit->date(), arg1);
    for (TableReservation * m : reserves) {
        QVariant var;
        var.setValue(*m);
        ui->reserveBox->addItem(m->getTime().toString(Qt::ISODate), var);
    }
}
