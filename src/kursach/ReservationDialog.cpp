#include "ReservationDialog.h"
#include "ui_ReservationDialog.h"

ReservationDialog::ReservationDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ReservationDialog)
{
    ui->setupUi(this);
    ui->dateEdit->setMinimumDate(QDate::currentDate());
}

ReservationDialog::~ReservationDialog()
{
    delete ui;
}

void ReservationDialog::setTableNumber(int id) {
    ui->tableNumber->setText("Table #" + QString::number(id));
}

void ReservationDialog::on_buttonBox_accepted()
{
    TableReservation * res = new TableReservation();
    res->setDate(ui->dateEdit->date());
    res->setTime(ui->timeEdit->time());
    emit reservedTable(res);
    close();
}

void ReservationDialog::on_dateEdit_dateChanged(const QDate &date)
{
    if (date == QDate::currentDate()) {
        ui->timeEdit->setMinimumTime(QTime::currentTime());
    }
    else
        ui->timeEdit->clearMinimumTime();
}
