#-------------------------------------------------
#
# Project created by QtCreator 2018-04-27T13:41:57
#
#-------------------------------------------------

QT       += core gui sql printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = kursach
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
    Storage.cpp \
    ReservationDialog.cpp \
    UserInfoDialog.cpp \
    Authorization.cpp \
    CashierAddDialog.cpp \
    HelpDialog.cpp \
    MainWindow.cpp \
    AddEditMenuDialog.cpp \
    EventInfoDialog.cpp

HEADERS += \
    Storage.h \
    EventDetailsDialog.h \
    AddMenuDialog.h \
    ReservationDialog.h \
    UserInfoDialog.h \
    Authorization.h \
    CashierAddDialog.h \
    HelpDialog.h \
    MainWindow.h

FORMS += \
    AddMenuDialog.ui \
    ReservationDialog.ui \
    UserInfoDialog.ui \
    Authorization.ui \
    EventDetailsDialog.ui \
    CashierAddDialog.ui \
    HelpDialog.ui \
    MainWindow.ui


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build-common-unknown-Release/release/ -lcommon
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-common-unknown-Release/debug/ -lcommon
else:unix: LIBS += -L$$PWD/../build-common-unknown-Release/ -lcommon

INCLUDEPATH += $$PWD/../common
DEPENDPATH += $$PWD/../common

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../build-common-unknown-Release/release/libcommon.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../build-common-unknown-Release/debug/libcommon.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../build-common-unknown-Release/release/common.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../build-common-unknown-Release/debug/common.lib
else:unix: PRE_TARGETDEPS += $$PWD/../build-common-unknown-Release/libcommon.a

RESOURCES += \
    ../images/images.qrc

STATECHARTS += \
    model.scxml
