#include "Authorization.h"
#include "ui_Authorization.h"
#include "MainWindow.h"
#include <QMessageBox>

Authorization::Authorization(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Authorization)
{
    ui->setupUi(this);

    QString path = "/home/nadiia/progbase3/databases";
    stor = new Storage(path);
    if(!stor->open()) {
        QMessageBox::critical(
                    this,
                    "Error opening database",
                    "Can't open database",
                    QMessageBox::Ok,
                    QMessageBox::Ok);
        return;
    }
    wind = new MainWindow();
    connect(wind, &MainWindow::logout, this, &Authorization::show);
}

Authorization::~Authorization()
{
    stor->close();
    delete stor;
    delete ui;
}

void Authorization::on_show_toggled(bool checked)
{
    if (checked)
        ui->password->setEchoMode(QLineEdit::Normal);
    else
        ui->password->setEchoMode(QLineEdit::Password);
}

void Authorization::on_login_textEdited(const QString &arg1)
{
    ui->loginButton->setEnabled(!ui->login->text().isEmpty() && !ui->password->text().isEmpty());
}

void Authorization::on_password_textEdited(const QString &arg1)
{
    ui->loginButton->setEnabled(!ui->login->text().isEmpty() && !ui->password->text().isEmpty());
}

void Authorization::on_loginButton_clicked()
{
    LoginData logData;
    logData.login = ui->login->text();
    logData.password_hash = ui->password->text();

    User * cur = stor->loginUser(logData);
    if (cur != nullptr) {
        wind->setUser(cur);
        wind->setStorage(stor);
        wind->show();
        ui->password->clear();
        this->close();
    }
    else {
        QMessageBox::critical(
                    this,
                    "Error entering",
                    "Wrong login or password",
                    QMessageBox::Ok,
                    QMessageBox::Ok);
    }
}
