#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "Storage.h"
#include "EventDetailsDialog.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->tabWidget->setCurrentWidget(ui->menuTab);
    ui->detailsWidget->setCurrentWidget(ui->picPage);

    createCategoryButtonGroup();
    createTableButtonGroup();
    ui->reservationTable->setColumnHidden(0, true);
    ui->reservationTable->setColumnHidden(1, true);
    ui->shiftTable->setColumnHidden(0, true);
    ui->shiftTable_2->setColumnHidden(0, true);

    ui->dateEdit->setMinimumDate(QDate::currentDate());

    ui->dateCashEdit->setDate(QDate::currentDate());
    ui->dateCashEdit->setMaximumDate(QDate::currentDate());

    QDate date = ui->calendarWidget->selectedDate();
    setEvents(date);

    QDate curDate = QDate::currentDate();
    int year = curDate.year();
    ui->yearBox->setMaximum(year);
    createPlot(curDate.month(), curDate.year());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setStorage(Storage * storage) {
    this->stor = storage;

    try {
        insertMenuToTable(stor->selectAllMenu());
    } catch (QSqlError & er) {
        QMessageBox::critical(
                    this,
                    "Error",
                    er.text(),
                    QMessageBox::Ok,
                    QMessageBox::Ok);
    }
    try {
        insertToWorkerList(stor->selectAllUsers());
    } catch (QSqlError & er) {
        QMessageBox::critical(
                    this,
                    "Error",
                    er.text(),
                    QMessageBox::Ok,
                    QMessageBox::Ok);
    }

    QHeaderView* header = ui->shiftTable->horizontalHeader();
    header->setSectionResizeMode(QHeaderView::Stretch);
    QHeaderView* header2 = ui->shiftTable_2->horizontalHeader();
    header2->setSectionResizeMode(QHeaderView::Stretch);
    insertToShiftTable();
    try {
        insertCashierToTable(stor->getCashierByDate(QDate::currentDate()));
    } catch (QSqlError & er) {
        QMessageBox::critical(
                    this,
                    "Error",
                    er.text(),
                    QMessageBox::Ok,
                    QMessageBox::Ok);
    }
}

void MainWindow::createPlot(int month, int year) {
    ui->customPlot->addGraph();
    ui->customPlot->graph(0)->setPen(QPen(Qt::blue)); // line color blue for first graph
    ui->customPlot->graph(0)->setBrush(QBrush(QColor(0, 0, 255, 20))); // first graph will be filled with translucent blue
    // generate some points of data (y0 for first, y1 for second graph):
    QVector<double> x(month), y(month);
    for (int i = 0; i < month; ++i)
    {
      x[i] = i + 1;
      try {
        y[i] = stor->getProfitByMonthAndYear(i + 1, year); // exponentially decaying cosine
      } catch (QSqlError & er) {
          QMessageBox::critical(
                      this,
                      "Error",
                      er.text(),
                      QMessageBox::Ok,
                      QMessageBox::Ok);
      }
    }
    // configure right and top axis to show ticks but no labels:
    // (see QCPAxisRect::setupFullAxesBox for a quicker method to do this)
    ui->customPlot->xAxis2->setVisible(true);
    ui->customPlot->xAxis2->setTickLabels(false);
    ui->customPlot->yAxis2->setVisible(true);
    ui->customPlot->yAxis2->setTickLabels(false);
    // make left and bottom axes always transfer their ranges to right and top axes:
    connect(ui->customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), ui->customPlot->xAxis2, SLOT(setRange(QCPRange)));
    connect(ui->customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), ui->customPlot->yAxis2, SLOT(setRange(QCPRange)));
    // pass data points to graphs:
    ui->customPlot->graph(0)->setData(x, y);
    // let the ranges scale themselves so graph 0 fits perfectly in the visible area:
    ui->customPlot->graph(0)->rescaleAxes();
    // same thing for graph 1, but only enlarge ranges (in case graph 1 is smaller than graph 0):
    // Note: we could have also just called ui->customPlot->rescaleAxes(); instead
    // Allow user to drag axis ranges with mouse, zoom with mouse wheel and select graphs by clicking:
    ui->customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);

    ui->customPlot->xAxis->setLabel("mounth");
    ui->customPlot->yAxis->setLabel("profit");
    // set axes ranges, so we see all data:
    ui->customPlot->replot();
}

void MainWindow::insertMenuToTable(QList<Menu *> menu) {
    ui->menuTable->setRowCount(0);
    for(auto &m: menu) {
        int newRowIndex = ui->menuTable->rowCount();
        ui->menuTable->insertRow(newRowIndex);

        QPixmap pic = m->getPic();
        QLabel * lbl = new QLabel;
        lbl->setPixmap(pic.scaled(70, 70, Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
        lbl->setAlignment(Qt::AlignCenter);
        ui->menuTable->setCellWidget(newRowIndex, 0, lbl);
        ui->menuTable->setItem(newRowIndex, 0, new QTableWidgetItem);
        QTableWidgetItem * item = new QTableWidgetItem();
        item->setText(m->getName());
        QVariant var;
        var.setValue(*m);
        item->setData(Qt::UserRole, var);
        ui->menuTable->setItem(newRowIndex, 1, item);
        ui->menuTable->resizeRowsToContents();
        ui->menuTable->resizeColumnToContents(0);
    }
}

void MainWindow::insertCashierToTable(QList<Cashier *> cash) {
    ui->cashierTable->setRowCount(0);
    ui->cashierTable->horizontalHeader()->show();
    for(auto &c: cash) {
        int newRowIndex = ui->cashierTable->rowCount();
        ui->cashierTable->insertRow(newRowIndex);

        QPixmap pic;
        if (c->getType() == 0)
            pic = QPixmap(":/icons/plus.png");
        else
            pic = QPixmap(":/icons/minus.png");
        QLabel * lbl = new QLabel;
        lbl->setPixmap(pic.scaled(16, 16, Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
        lbl->setAlignment(Qt::AlignCenter);
        ui->cashierTable->setCellWidget(newRowIndex, 0, lbl);
        ui->cashierTable->setItem(newRowIndex, 0, new QTableWidgetItem);

        QTableWidgetItem * sumItem = new QTableWidgetItem();
        sumItem->setTextAlignment(Qt::AlignCenter);
        sumItem->setText(QString::number(c->getSum()) + " $");
        ui->cashierTable->setItem(newRowIndex, 1, sumItem);

        QTableWidgetItem * targetItem = new QTableWidgetItem();
        targetItem->setText(c->getTarget());
        ui->cashierTable->setItem(newRowIndex, 2, targetItem);

        ui->cashierTable->resizeColumnToContents(0);
        ui->cashierTable->resizeColumnToContents(1);
    }
}

void MainWindow::setUser(User *us) {
    user = us;
    ui->fullName->setText("You entered as " + us->getFullName());
    int tabCount = ui->tabWidget->count();
    for (int i = 3; i < tabCount; i++) {
        ui->tabWidget->removeTab(3);
    }
    switch ((int)user->getPostType()) {
    case 0: { // boss
        ui->tabWidget->insertTab(3, ui->hallTab, "Hall");
        ui->tabWidget->insertTab(4, ui->staffTab, "Staff");
        ui->tabWidget->insertTab(5, ui->profitTab, "Profit plot");
        ui->addMenuButton->hide();
        ui->editMenuButton->hide();
        ui->removeMenuButton->hide();
        ui->addEventButton->hide();
        ui->removeEventButton->hide();
        ui->reserveButton->hide();
        ui->cancelReservButton->hide();
        ui->profitButton->hide();

        ui->lossButton->show();
        ui->addWorkerButton->show();
        ui->removeWorkerButton->show();
        break;
    }
    case 1: { // admin
        ui->tabWidget->insertTab(3, ui->hallTab, "Hall");
        ui->tabWidget->insertTab(4, ui->scheduleTab, "Work schedule");
        ui->tabWidget->insertTab(5, ui->staffTab, "Staff");
        ui->lossButton->hide();
        ui->addWorkerButton->hide();
        ui->removeWorkerButton->hide();

        ui->profitButton->show();
        ui->addMenuButton->show();
        ui->editMenuButton->show();
        ui->removeMenuButton->show();
        ui->addEventButton->show();
        ui->removeEventButton->show();
        ui->reserveButton->show();
        ui->cancelReservButton->show();
        break;
    }
    case 2: { // waiter
        ui->tabWidget->insertTab(3, ui->hallTab, "Hall");
        ui->tabWidget->insertTab(4, ui->scheduleTab, "Work schedule");
        ui->addMenuButton->hide();
        ui->editMenuButton->hide();
        ui->removeMenuButton->hide();
        ui->addEventButton->hide();
        ui->removeEventButton->hide();
        ui->reserveButton->hide();
        ui->cancelReservButton->hide();
        ui->lossButton->hide();

        ui->profitButton->show();
        break;
    }
    case 3: { // barman
        ui->tabWidget->insertTab(3, ui->scheduleTab, "Work schedule");
        ui->addMenuButton->hide();
        ui->editMenuButton->hide();
        ui->removeMenuButton->hide();
        ui->addEventButton->hide();
        ui->removeEventButton->hide();
        ui->reserveButton->hide();
        ui->cancelReservButton->hide();
        ui->lossButton->hide();

        ui->profitButton->show();
        break;
    }
    default:
        break;
    }
}

void MainWindow::createCategoryButtonGroup() {
    categoryGroup = new QButtonGroup(this);
    categoryGroup->addButton(ui->allButton, 0);
    categoryGroup->addButton(ui->beerButton, 1);
    categoryGroup->addButton(ui->champagneButton, 2);
    categoryGroup->addButton(ui->wineButton, 3);
    categoryGroup->addButton(ui->brandyButton, 4);
    categoryGroup->addButton(ui->whiskeyButton, 5);
    categoryGroup->addButton(ui->tequilaButton, 6);
    categoryGroup->addButton(ui->cocktailsButton, 7);
    categoryGroup->addButton(ui->softDrinksButton, 8);
    categoryGroup->addButton(ui->teaButton, 9);
    categoryGroup->addButton(ui->coffeeButton, 10);
    categoryGroup->addButton(ui->snacksButton, 11);
    categoryGroup->addButton(ui->dessertsButton, 12);
    connect(categoryGroup, SIGNAL(buttonClicked(int)), this, SLOT(categoryChanged(int)));
}

void MainWindow::createTableButtonGroup() {
    tableGroup = new QButtonGroup(this);
    tableGroup->addButton(ui->table1, 1);
    tableGroup->addButton(ui->table2, 2);
    tableGroup->addButton(ui->table3, 3);
    tableGroup->addButton(ui->table4, 4);
    tableGroup->addButton(ui->table5, 5);
    tableGroup->addButton(ui->table6, 6);
    tableGroup->addButton(ui->table7, 7);
    tableGroup->addButton(ui->table8, 8);
    tableGroup->addButton(ui->table9, 9);
    tableGroup->addButton(ui->table10, 10);
    tableGroup->addButton(ui->table11, 11);
    tableGroup->addButton(ui->table12, 12);
    connect(tableGroup, SIGNAL(buttonClicked(int)), this, SLOT(tableSelectionChanged(int)));
}

void MainWindow::on_menuTable_itemSelectionChanged()
{
    QList<QTableWidgetItem *> items = ui->menuTable->selectedItems();
    if (items.count() == 0) {
        ui->detailsWidget->setCurrentWidget(ui->picPage);
        ui->editMenuButton->setEnabled(false);
        ui->removeMenuButton->setEnabled(false);
    }
    else {
        ui->editMenuButton->setEnabled(true);
        ui->removeMenuButton->setEnabled(true);
        ui->detailsWidget->setCurrentWidget(ui->detailsPage);

        QTableWidgetItem * item = items.at(1);
        QVariant var = item->data(Qt::UserRole);
        Menu m = var.value<Menu>();
        ui->nameEdit->setText(m.getName());
        ui->costEdit->setText(QString::number(m.getCost()) + " $");
        ui->ingredients->setText(m.getIngrid());
    }
}

void MainWindow::categoryChanged(int buttonId) {
    ui->searchEdit->clear();
    if (buttonId == 0) {
        try {
            insertMenuToTable(stor->selectAllMenu());
        } catch (QSqlError & er) {
            QMessageBox::critical(
                        this,
                        "Error",
                        er.text(),
                        QMessageBox::Ok,
                        QMessageBox::Ok);
        }
    }
    else {
        try {
            insertMenuToTable(stor->selectCategory((Category)(buttonId - 1)));
        } catch (QSqlError & er) {
            QMessageBox::critical(
                        this,
                        "Error",
                        er.text(),
                        QMessageBox::Ok,
                        QMessageBox::Ok);
        }
    }
}

void MainWindow::on_searchButton_clicked()
{
    if (ui->allButton->isChecked()) {
        try {
            insertMenuToTable(stor->findMenuByName(ui->searchEdit->text()));
        } catch (QSqlError & er) {
            QMessageBox::critical(
                        this,
                        "Error",
                        er.text(),
                        QMessageBox::Ok,
                        QMessageBox::Ok);
        }
    }
    else {
        try {
            insertMenuToTable(stor->findMenuByNameAndCategory(ui->searchEdit->text(), categoryGroup->checkedId()));
        } catch (QSqlError & er) {
            QMessageBox::critical(
                        this,
                        "Error",
                        er.text(),
                        QMessageBox::Ok,
                        QMessageBox::Ok);
        }
    }
}

void MainWindow::setEvents(const QDate &date) {
    ui->eventTable->setRowCount(0);

    QDate selectedDate = date;
    ui->dateLabel->setText(selectedDate.toString());
    QList<Event *> events;
    try {
        events = stor->getEventsByDate(selectedDate);
    } catch (QSqlError & er) {
        QMessageBox::critical(
                    this,
                    "Error",
                    er.text(),
                    QMessageBox::Ok,
                    QMessageBox::Ok);
    }

    if (events.count() == 0) {
        int row = ui->eventTable->rowCount();
        ui->eventTable->insertRow(row);
        QTableWidgetItem * event = new QTableWidgetItem();
        event->setText("No events");
        ui->eventTable->setItem(row, 0, event);
    }
    else for (Event * e: events) {
        int newRowIndex = ui->eventTable->rowCount();
        ui->eventTable->insertRow(newRowIndex);
        QTableWidgetItem * event = new QTableWidgetItem();
        event->setText(e->getName());
        QVariant var;
        var.setValue(*e);
        event->setData(Qt::UserRole, var);
        ui->eventTable->setItem(newRowIndex, 0, event);
    }
}

void MainWindow::on_calendarWidget_clicked(const QDate &date)
{
    setEvents(date);
}

void MainWindow::on_eventTable_doubleClicked(const QModelIndex &index)
{
    int row = index.row();
    QTableWidgetItem * item = ui->eventTable->item(row, 0);
    QVariant var = item->data(Qt::UserRole);
    if (var.canConvert<Event>()) {
        Event ev = var.value<Event>();

        EventDetailsDialog d;
        connect(&d, SIGNAL(editEvent(Event *)), this, SLOT(eventEdit(Event *)));
        d.setWindowTitle("EventInfo");
        d.setData(ev, View, user);
        d.exec();
    }
}

void MainWindow::addNewMenu(Menu * menu) {
    try {
        stor->addMenu(menu);
        int id = categoryGroup->checkedId();
        if (id == 0)
            insertMenuToTable(stor->selectAllMenu());
        else
            insertMenuToTable(stor->selectCategory((Category)(id - 1)));
    } catch (QSqlError & er) {
        QMessageBox::critical(
                    this,
                    "Error",
                    er.text(),
                    QMessageBox::Ok,
                    QMessageBox::Ok);
    }
}

void MainWindow::on_addMenuButton_clicked()
{
    AddMenuDialog d;
    d.setWindowTitle("Add to menu");
    d.setAction(Add);
    connect(&d, SIGNAL(addMenu(Menu *)), this, SLOT(addNewMenu(Menu *)));
    d.exec();
}

void MainWindow::on_removeMenuButton_clicked()
{
    QList<QTableWidgetItem *> items = ui->menuTable->selectedItems();
    if (items.count() == 2) {
        QTableWidgetItem * item = items.at(1);
        QVariant var = item->data(Qt::UserRole);
        Menu m = var.value<Menu>();
        try {
            stor->removeFromMenu(m.getId());
            ui->menuTable->removeRow(item->row());
        } catch (QSqlError & er) {
            QMessageBox::critical(
                        this,
                        "Error",
                        er.text(),
                        QMessageBox::Ok,
                        QMessageBox::Ok);
        }
    }
}

void MainWindow::editMenu(Menu * menu) {
    try {
        stor->editMenu(menu);
        insertMenuToTable(stor->selectAllMenu());
        ui->allButton->setChecked(true);
    } catch (QSqlError & er) {
        QMessageBox::critical(
                    this,
                    "Error",
                    er.text(),
                    QMessageBox::Ok,
                    QMessageBox::Ok);
    }
}

void MainWindow::on_editMenuButton_clicked()
{
    QList<QTableWidgetItem *> items = ui->menuTable->selectedItems();
    if (items.count() == 2) {
        QTableWidgetItem * item = items.at(1);
        QVariant var = item->data(Qt::UserRole);
        Menu m = var.value<Menu>();
        AddMenuDialog d;
        d.setWindowTitle("Edit menu item");
        d.setAction(Edit);
        d.setMenu(m);
        connect(&d, SIGNAL(menuEdited(Menu *)), this, SLOT(editMenu(Menu *)));
        d.exec();
    }
}

void MainWindow::eventAdd(Event * event) {
    event->setDate(ui->calendarWidget->selectedDate());
    try {
        stor->addEvent(event);
        setEvents(event->getDate());
    } catch (QSqlError & er) {
        QMessageBox::critical(
                    this,
                    "Error",
                    er.text(),
                    QMessageBox::Ok,
                    QMessageBox::Ok);
    }
}

void MainWindow::eventEdit(Event * event) {
    event->setDate(ui->calendarWidget->selectedDate());
    try {
        stor->editEvent(event);
        setEvents(event->getDate());
    } catch (QSqlError & er) {
        QMessageBox::critical(
                    this,
                    "Error",
                    er.text(),
                    QMessageBox::Ok,
                    QMessageBox::Ok);
    }
}

void MainWindow::on_addEventButton_clicked()
{
    EventDetailsDialog d;
    d.setWindowTitle("Add event");
    d.setData(Event(), Add, user);
    connect(&d, SIGNAL(addEvent(Event *)), this, SLOT(eventAdd(Event *)));
    d.exec();
}

void MainWindow::insertReservationToTable(QList<TableReservation *> reserv) {
    ui->reservationTable->setRowCount(0);
    for(auto &m: reserv) {
        int newRowIndex = ui->reservationTable->rowCount();
        ui->reservationTable->insertRow(newRowIndex);

        QTableWidgetItem * idItem = new QTableWidgetItem();
        idItem->setTextAlignment(Qt::AlignCenter);
        idItem->setText(QString::number(m->getId()));
        ui->reservationTable->setItem(newRowIndex, 0, idItem);

        QTableWidgetItem * tableItem = new QTableWidgetItem();
        tableItem->setTextAlignment(Qt::AlignCenter);
        tableItem->setText(QString::number(m->getTable()));
        ui->reservationTable->setItem(newRowIndex, 1, tableItem);

        QTableWidgetItem * dateItem = new QTableWidgetItem();
        dateItem->setTextAlignment(Qt::AlignCenter);
        dateItem->setText(m->getDate().toString(Qt::ISODate));
        ui->reservationTable->setItem(newRowIndex, 2, dateItem);

        QTableWidgetItem * timeItem = new QTableWidgetItem();
        timeItem->setText(m->getTime().toString("hh:mm"));
        ui->reservationTable->setItem(newRowIndex, 3, timeItem);
    }
}

void MainWindow::tableSelectionChanged(int tableId) {
    setDefaultTablesColor();
    ui->reservationTable->setColumnHidden(1, true);
    ui->reserveButton->setEnabled(true);
    ui->tableNumber->setText(QString::number(tableId));
    QHeaderView* header = ui->reservationTable->horizontalHeader();
    header->setSectionResizeMode(QHeaderView::Stretch);
    try {
        insertReservationToTable(stor->getReservationByTableId(tableId));
        ui->thisTableButton->setEnabled(true);
    } catch (QSqlError & er) {
        QMessageBox::critical(
                    this,
                    "Error",
                    er.text(),
                    QMessageBox::Ok,
                    QMessageBox::Ok);
    }
}

void MainWindow::setDefaultTablesColor() {
    QString styleSheet = "background-color: rgb(164, 0, 0);";
    ui->table1->setStyleSheet(styleSheet);
    ui->table2->setStyleSheet(styleSheet);
    ui->table3->setStyleSheet(styleSheet);
    ui->table4->setStyleSheet(styleSheet);
    ui->table5->setStyleSheet(styleSheet);
    ui->table6->setStyleSheet(styleSheet);
    ui->table7->setStyleSheet(styleSheet);
    ui->table8->setStyleSheet(styleSheet);
    ui->table9->setStyleSheet(styleSheet);
    ui->table10->setStyleSheet(styleSheet);
    ui->table11->setStyleSheet(styleSheet);
    ui->table12->setStyleSheet(styleSheet);
}

void MainWindow::on_allTablesButton_clicked()
{
    ui->tableNumber->setText("all");
    ui->reservationTable->setColumnHidden(1, false);
    QHeaderView* header = ui->reservationTable->horizontalHeader();
    header->setSectionResizeMode(QHeaderView::ResizeToContents);
    QList<TableReservation *> list;
    try {
        list = stor->getReservationByDate(ui->dateEdit->date());
        insertReservationToTable(list);
        for (TableReservation * t: list) {
            tableGroup->button(t->getTable())->setStyleSheet("background-color: rgb(245, 121, 0);");
        }
    } catch (QSqlError & er) {
        QMessageBox::critical(
                    this,
                    "Error",
                    er.text(),
                    QMessageBox::Ok,
                    QMessageBox::Ok);
    }
}

void MainWindow::on_eventTable_itemSelectionChanged()
{
    QList<QTableWidgetItem *> items = ui->eventTable->selectedItems();
    if (items.count() == 0)
        ui->removeEventButton->setEnabled(false);
    else {
        QTableWidgetItem * item = items.at(0);
        QVariant var = item->data(Qt::UserRole);
        if (var.canConvert<Event>())
            ui->removeEventButton->setEnabled(true);
        else
            ui->removeEventButton->setEnabled(false);
    }
}

void MainWindow::on_removeEventButton_clicked()
{
    QList<QTableWidgetItem *> items = ui->eventTable->selectedItems();
    if (items.count() == 1) {
        QTableWidgetItem * item = items.at(0);
        QVariant var = item->data(Qt::UserRole);
        Event e = var.value<Event>();
        try {
            stor->removeEvent(e.getId());
            ui->eventTable->removeRow(item->row());
        } catch (QSqlError & er) {
            QMessageBox::critical(
                        this,
                        "Error",
                        er.text(),
                        QMessageBox::Ok,
                        QMessageBox::Ok);
        }
    }
    setEvents(ui->calendarWidget->selectedDate());
}

void MainWindow::on_cancelReservButton_clicked()
{
    QList<QTableWidgetItem *> items = ui->reservationTable->selectedItems();
    if (items.count() > 0) {
        QTableWidgetItem * item = items.at(0);
        int row = item->row();
        int id = ui->reservationTable->item(row, 0)->text().toInt();
        int table = ui->reservationTable->item(row, 1)->text().toInt();
        QList<TableReservation *> tables;
        try {
            stor->removeReserv(id);
            ui->reservationTable->removeRow(row);
            tables = stor->getReservationByDateAndTable(ui->dateEdit->date(), table);
            if (tables.isEmpty()) {
                tableGroup->button(table)->setStyleSheet("background-color: rgb(164, 0, 0);");
            }
        } catch (QSqlError & er) {
            QMessageBox::critical(
                        this,
                        "Error",
                        er.text(),
                        QMessageBox::Ok,
                        QMessageBox::Ok);
        }
    }
}

void MainWindow::on_reservationTable_itemSelectionChanged()
{
    QList<QTableWidgetItem *> items = ui->reservationTable->selectedItems();
    if (items.count() == 0)
        ui->cancelReservButton->setEnabled(false);
    else
        ui->cancelReservButton->setEnabled(true);
}

void MainWindow::on_thisTableButton_clicked()
{
    setDefaultTablesColor();
    int tableId = tableGroup->checkedId();
    ui->tableNumber->setText(QString::number(tableId));
    ui->reservationTable->setColumnHidden(1, true);
    QHeaderView* header = ui->reservationTable->horizontalHeader();
    header->setSectionResizeMode(QHeaderView::Stretch);
    try {
        insertReservationToTable(stor->getReservationByDateAndTable(ui->dateEdit->date(), tableId));
    } catch (QSqlError & er) {
        QMessageBox::critical(
                    this,
                    "Error",
                    er.text(),
                    QMessageBox::Ok,
                    QMessageBox::Ok);
    }
}

void MainWindow::addNewReservation(TableReservation * res) {
    res->setTable(tableGroup->checkedId());
    int id;
    try {
        id = stor->addReservation(res);
        tableSelectionChanged(tableGroup->checkedId());
        QMessageBox::information(
                    this,
                    "New reservation successfully added",
                    "Reservation id: " + QString::number(id),
                    QMessageBox::Ok,
                    QMessageBox::Ok);
    } catch (QSqlError & er) {
        QMessageBox::critical(
                    this,
                    "Error",
                    er.text(),
                    QMessageBox::Ok,
                    QMessageBox::Ok);
    }
}

void MainWindow::on_reserveButton_clicked()
{
    ReservationDialog d;
    connect(&d, SIGNAL(reservedTable(TableReservation *)), this, SLOT(addNewReservation(TableReservation *)));
    d.setWindowTitle("Reservation");
    d.setTableNumber(tableGroup->checkedId());
    d.exec();
}

void MainWindow::on_reservationTable_doubleClicked(const QModelIndex &index)
{
    int row = index.row();
    QTableWidgetItem * item = ui->reservationTable->item(row, 0);
    int id = item->text().toInt();
    QMessageBox::information(
                this,
                "Reservation",
                "Reservation id: " + QString::number(id),
                QMessageBox::Ok,
                QMessageBox::Ok);
}


void MainWindow::insertToWorkerList(QList<User *> users) {
    ui->workerList->clear();
    for (User *l : users) {
        if (user->getPostType() != Admin || l->getPostType() != BigBoss) {
            QListWidgetItem *item = new QListWidgetItem;
            item->setText(l->getFullName());
            QVariant var;
            var.setValue(*l);
            item->setData(Qt::UserRole, var);
            ui->workerList->addItem(item);
        }
    }
}

void MainWindow::insertToShiftTable() {
    ui->shiftTable->setRowCount(0);
    ui->shiftTable_2->setRowCount(0);
    QList<User *> users;
    try {
        users = stor->selectAllUsers();
    } catch (QSqlError & er) {
        QMessageBox::critical(
                    this,
                    "Error",
                    er.text(),
                    QMessageBox::Ok,
                    QMessageBox::Ok);
    }
    for (User * u : users) {
        if (u->getPostType() != BigBoss && user->getPostType() != u->getPostType()) {
            int newRowIndex = ui->shiftTable->rowCount();
            ui->shiftTable->insertRow(newRowIndex);
            ui->shiftTable->setVerticalHeaderItem(newRowIndex, new QTableWidgetItem(u->getFullName()));
            QTableWidgetItem * idItem = new QTableWidgetItem();
            idItem->setText(QString::number(u->getId()));
            ui->shiftTable->setItem(newRowIndex, 0, idItem);
            QList<int> days;
            try {
                days = stor->getWorkScheduleByUserId(u->getId());
            } catch (QSqlError & er) {
                QMessageBox::critical(
                            this,
                            "Error",
                            er.text(),
                            QMessageBox::Ok,
                            QMessageBox::Ok);
            }
            for (int &dayId : days) {
                QPixmap pic(":/icons/checked.png");
                QLabel * lbl = new QLabel;
                lbl->setPixmap(pic.scaled(20, 20, Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
                lbl->setAlignment(Qt::AlignCenter);
                ui->shiftTable->setCellWidget(newRowIndex, dayId, lbl);
                ui->shiftTable->setItem(newRowIndex, dayId, new QTableWidgetItem);
            }
        }
        if (u->getPostType() != BigBoss) {
            int newRowIndex = ui->shiftTable_2->rowCount();
            ui->shiftTable_2->insertRow(newRowIndex);
            ui->shiftTable_2->setVerticalHeaderItem(newRowIndex, new QTableWidgetItem(u->getFullName()));
            QList<int> days;
            try {
                days = stor->getWorkScheduleByUserId(u->getId());
            } catch (QSqlError & er) {
                QMessageBox::critical(
                            this,
                            "Error",
                            er.text(),
                            QMessageBox::Ok,
                            QMessageBox::Ok);
            }
            for (int &dayId : days) {
                QPixmap pic(":/icons/checked.png");
                QLabel * lbl = new QLabel;
                lbl->setPixmap(pic.scaled(20, 20, Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
                lbl->setAlignment(Qt::AlignCenter);
                ui->shiftTable_2->setCellWidget(newRowIndex, dayId, lbl);
            }
        }
    }
}


void MainWindow::on_uncheckedButton_clicked()
{
    int curRow = ui->shiftTable->currentRow();
    int curColumn = ui->shiftTable->currentColumn();
    QTableWidgetItem * idItem = ui->shiftTable->item(curRow, 0);
    int id = idItem->text().toInt();
    try {
        stor->removeFromWorkSchedule(id, curColumn);
        ui->shiftTable->removeCellWidget(curRow, curColumn);
        auto item = ui->shiftTable->takeItem(curRow, curColumn);
        delete item;
        emit ui->shiftTable->itemSelectionChanged();
    } catch (QSqlError & er) {
        QMessageBox::critical(
                    this,
                    "Error",
                    er.text(),
                    QMessageBox::Ok,
                    QMessageBox::Ok);
    }
}

void MainWindow::on_checkedButton_clicked()
{
    int curRow = ui->shiftTable->currentRow();
    int curColumn = ui->shiftTable->currentColumn();
    QTableWidgetItem * idItem = ui->shiftTable->item(curRow, 0);
    int id = idItem->text().toInt();
    try {
        stor->addToWorkSchedule(id, curColumn);
        QPixmap pic(":/icons/checked.png");
        QLabel * lbl = new QLabel;
        lbl->setPixmap(pic.scaled(20, 20, Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
        lbl->setAlignment(Qt::AlignCenter);
        ui->shiftTable->setCellWidget(curRow, curColumn, lbl);
        ui->shiftTable->setItem(curRow, curColumn, new QTableWidgetItem);
        emit ui->shiftTable->itemSelectionChanged();
    } catch (QSqlError & er) {
        QMessageBox::critical(
                    this,
                    "Error",
                    er.text(),
                    QMessageBox::Ok,
                    QMessageBox::Ok);
    }
}

void MainWindow::on_shiftTable_itemSelectionChanged()
{
    if (!ui->shiftTable->item(ui->shiftTable->currentRow(), ui->shiftTable->currentColumn())) {
        ui->uncheckedButton->setEnabled(false);
        ui->checkedButton->setEnabled(true);
    }
    else {
        ui->uncheckedButton->setEnabled(true);
        ui->checkedButton->setEnabled(false);
    }
}

void MainWindow::userEdit(User *us) {
    try {
        stor->editUser(us);
        insertToWorkerList(stor->selectAllUsers());
        insertToShiftTable();
    } catch (QSqlError & er) {
        QMessageBox::critical(
                    this,
                    "Error",
                    er.text(),
                    QMessageBox::Ok,
                    QMessageBox::Ok);
    }
}

void MainWindow::loginDataEdit(LoginData log) {
    try {
        stor->editLoginData(log);
    } catch (QSqlError & er) {
        QMessageBox::critical(
                    this,
                    "Error",
                    er.text(),
                    QMessageBox::Ok,
                    QMessageBox::Ok);
    }
}

void MainWindow::on_workerList_doubleClicked(const QModelIndex &index)
{
    QListWidgetItem * item = ui->workerList->item(index.row());
    QVariant var = item->data(Qt::UserRole);
    User u = var.value<User>();

    UserInfoDialog d;
    d.setWindowTitle("User info");
    connect(&d, SIGNAL(editUser(User *)), this, SLOT(userEdit(User *)));
    connect(&d, SIGNAL(editLogin(LoginData)), this, SLOT(loginDataEdit(LoginData)));
    try {
        d.setData(u, stor->getLoginDataByUserId(u.getId()), View, user, stor);
        d.exec();
    } catch (QSqlError & er) {
        QMessageBox::critical(
                    this,
                    "Error",
                    er.text(),
                    QMessageBox::Ok,
                    QMessageBox::Ok);
    }
}

void MainWindow::userAdd(User * user, LoginData log) {
    try {
        stor->addUser(user, log);
        insertToWorkerList(stor->selectAllUsers());
        insertToShiftTable();
    } catch (QSqlError & er) {
        QMessageBox::critical(
                    this,
                    "Error",
                    er.text(),
                    QMessageBox::Ok,
                    QMessageBox::Ok);
    }
}

void MainWindow::on_addWorkerButton_clicked()
{
    UserInfoDialog d;
    d.setWindowTitle("Add user");
    try {
        d.setData(User(), LoginData(), Add, user, stor);
        connect(&d, SIGNAL(addUser(User*,LoginData)), this, SLOT(userAdd(User *, LoginData)));
        d.exec();
    } catch (QSqlError & er) {
        QMessageBox::critical(
                    this,
                    "Error",
                    er.text(),
                    QMessageBox::Ok,
                    QMessageBox::Ok);
    }
}

void MainWindow::on_removeWorkerButton_clicked()
{
    QList<QListWidgetItem *> items = ui->workerList->selectedItems();
    if (items.count() > 0) {
        QListWidgetItem * item = items.at(0);
        QVariant var = item->data(Qt::UserRole);
        User e = var.value<User>();
        try {
            stor->removeUser(e.getId());
            QListWidgetItem * toRemove = ui->workerList->takeItem(ui->workerList->row(item));
            delete toRemove;
            insertToShiftTable();
        } catch (QSqlError & er) {
            QMessageBox::critical(
                        this,
                        "Error",
                        er.text(),
                        QMessageBox::Ok,
                        QMessageBox::Ok);
        }
    }
}

void MainWindow::on_workerList_itemSelectionChanged()
{
    QList<QListWidgetItem *> items = ui->workerList->selectedItems();
    if (items.count() == 0)
        ui->removeWorkerButton->setEnabled(false);
    else
        ui->removeWorkerButton->setEnabled(true);
}

void MainWindow::cashierAdd(int type, QString target, double sum) {
    QDate date = ui->dateCashEdit->date();
    Cashier * cash = new Cashier(type, sum, target, date);
    try {
        stor->addToCashier(cash);
        insertCashierToTable(stor->getCashierByDate(date));
    } catch (QSqlError & er) {
        QMessageBox::critical(
                    this,
                    "Error",
                    er.text(),
                    QMessageBox::Ok,
                    QMessageBox::Ok);
    }
}

void MainWindow::on_profitButton_clicked()
{
    CashierAddDialog d;
    try {
        d.setData(user, stor);
        connect(&d, SIGNAL(addCash(int, QString, double)), this, SLOT(cashierAdd(int, QString, double)));
        d.exec();
    } catch (QSqlError & er) {
        QMessageBox::critical(
                    this,
                    "Error",
                    er.text(),
                    QMessageBox::Ok,
                    QMessageBox::Ok);
    }
}

void MainWindow::on_lossButton_clicked()
{
    CashierAddDialog d;
    try {
        d.setData(user, stor);
        connect(&d, SIGNAL(addCash(int, QString, double)), this, SLOT(cashierAdd(int, QString, double)));
        d.exec();
    } catch (QSqlError & er) {
        QMessageBox::critical(
                    this,
                    "Error",
                    er.text(),
                    QMessageBox::Ok,
                    QMessageBox::Ok);
    }
}

void MainWindow::on_dateCashEdit_dateChanged(const QDate &date)
{
    try {
        insertCashierToTable(stor->getCashierByDate(date));
    } catch (QSqlError & er) {
        QMessageBox::critical(
                    this,
                    "Error",
                    er.text(),
                    QMessageBox::Ok,
                    QMessageBox::Ok);
    }
}

void MainWindow::on_yearBox_valueChanged(int arg1)
{
    QDate curDate = QDate::currentDate();
    if (curDate.year() == arg1) {
        createPlot(curDate.month(), arg1);
    }
    else {
        createPlot(12, arg1);
    }
}

void MainWindow::on_helpButton_clicked()
{
    HelpDialog d;
    d.exec();
}

void MainWindow::on_exitButton_clicked()
{
    this->close();
    emit logout();
}

void MainWindow::on_printButton_clicked()
{
    QPrinter printer;
    QPrintDialog d(&printer, this);
    d.setWindowTitle(tr("Print Document"));

    if (d.exec() != QDialog::Accepted) {
        return;
    }
    QPainter painter;
    painter.begin(&printer);
    double xscale = printer.pageRect().width()/double(ui->widget->width());
    double yscale = printer.pageRect().height()/double(ui->widget->height());
    double scale = qMin(xscale, yscale);
    painter.translate(printer.paperRect().x() + printer.pageRect().width()/2,
                    printer.paperRect().y() + printer.pageRect().height()/2);
    painter.scale(scale, scale);
    painter.translate(-width()/2, -height()/2);
    ui->widget->render(&painter);
}
