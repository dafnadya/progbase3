#ifndef EVENTDETAILSDIALOG_H
#define EVENTDETAILSDIALOG_H

#include <QDialog>
#include "Event.h"
#include "Actions.h"
#include "User.h"

namespace Ui {
class EventDetailsDialog;
}

class EventDetailsDialog : public QDialog
{
    Q_OBJECT

    int id;

    Action action;

public:
    explicit EventDetailsDialog(QWidget *parent = 0);
    ~EventDetailsDialog();

    void setData(Event ev, Action act, User *user);

private slots:
    void on_okButton_clicked();

    void on_editButton_clicked();

    void on_okEditButton_clicked();

    void on_cancelButton_clicked();

    void on_nameEdit_textEdited(const QString &arg1);

    void enableOkButton();

    void on_timeEdit_timeChanged(const QTime &time);

    void on_costEdit_valueChanged(double arg1);

    void on_detailsEdit_textChanged();

private:
    Ui::EventDetailsDialog *ui;
signals:
    void addEvent(Event * ev);
    void editEvent(Event * ev);
};

#endif // EVENTDETAILSDIALOG_H
